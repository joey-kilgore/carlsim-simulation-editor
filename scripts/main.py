from simulation import Simulation
import sys

sim = Simulation('data\\networkHW.dat', debug=True)
sim.save('data\\networkHW_2.dat', debug=True)
print('# syn: '+str(len(sim.synapses)))

# sample code to find largest weight
maxWeight = sim.synapses[0][0].weight
for syn in sim.synapses[0]:
    if syn.weight > maxWeight:
        maxWeight=syn.weight
print('WEIGHT BYTES: '+str(sys.getsizeof(sim.synapses[0][0].weight)))
print('MAX WEIGHT: '+str(maxWeight))

# sample code for changing a single weight
print('CHANGE A WEIGHT VALUE TO 0')
print('PRE: '+str(sim.synapses[0][0].weight))
sim.synapses[0][0].weight = 0
print('POST: '+str(sim.synapses[0][0].weight))
sim.save('data\\newNetwork.dat',debug=True)

# set all recurrent connections to 0
for syn in sim.synapses[0]:
    if(syn.gGrpIdPre == syn.gGrpIdPost):
        syn.weight=0
sim.save('data\\noRecurrent.dat',debug=True)

# set all connection weights to 0
for syn in sim.synapses[0]:
    syn.weight = 0
sim.save('data\\zeroNetwork.dat',debug=True)
