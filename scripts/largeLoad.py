from simulation import Simulation

sim = Simulation('data\\networkZ.dat', debug=True)
for syn in sim.synapses[0]:
    if(syn.gGrpIdPre == syn.gGrpIdPost):
        syn.weight=0
sim.save('data\\networkZ_2.dat')
